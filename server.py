import socket
from _thread import *
import pickle
import sqlite3



server = ""
port = 44444
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((server, port))
except socket.error as e:
    str(e)

s.listen()
print("Serveur démarré")

connected = dict()
allPositions = dict()

def threaded_client(conn):
    global allPositions
    conne = sqlite3.connect("basededonnees.db")
    cursor = conne.cursor()

    idConn = conn.getpeername()[1]
    conn.send(b'Connecte !')
    while True:
        try:
            data = pickle.loads(conn.recv(2048))
            if 'name' in data:
                if (data['name'],) not in cursor.execute(f'select pseudo from users').fetchall():
                    name = data['name']
                    cursor.execute(f'insert into users (pseudo, role) values ("{name}", "users" )')
                    conne.commit()
                    response = cursor.execute(f'select * from users where pseudo = "{name}"').fetchone()
                    conn.send(pickle.dumps([response[1], response[2]]))
                else:
                    name = data['name']
                    response = cursor.execute(f'select * from users where pseudo = "{name}"').fetchone()
                    conn.send(pickle.dumps([response[1], response[2]]))
            if 'crud' in data:
                response = cursor.execute(f'select * from potions').fetchall()
                conn.send(pickle.dumps(response))
            if 'pos' in data:
                allPositions[idConn] = [data['pos'].x, data['pos'].y]
                for id, connect in connected.items():
                    try:
                        connect[1].send(pickle.dumps(allPositions, protocol=2))
                    except Exception as e:
                        pass

        except Exception as e:
            print("ERROR: " + str(e))
            break

    print("Connection lost")
    del allPositions[idConn]
    conn.close()


while True:
    conn, addr = s.accept()
    print("Connecté à :", addr)

    connected[addr[1]] = [addr[0], conn]
    start_new_thread(threaded_client, (conn,))

