# Projet-dev-logiciel

## Présentation

## Sommaire

- [Projet-dev-logiciel](#projet-dev-logiciel)
  - [Présentation](#présentation)
  - [Sommaire](#sommaire)
    - [Dépôt Git](#dépôt-git)
    - [Diaporama](#diaporama)
  - [Initialisation du projet](#initialisation-du-projet)
    - [Pré-requis](#pré-requis)
  - [Installation du projet](#installation-du-projet)
    - [Clonage du dépôt git](#clonage-du-dépôt-git)
    - [Installation des dépendances](#installation-des-dépendances)
    - [Activer le serveur](#activer-le-serveur)
    - [Lancer le menu](#lancer-le-menu)
  - [Architecture du projet](#architecture-du-projet)
    - [Images](#images)
    - [Le projet](#le-projet)
  - [Le jeu](#le-jeu)
  - [Les Fonctionnalitées](#les-fonctionnalitées)
    - [Le menu](#le-menu)
    - [La Base de donnée](#la-base-de-donnée)
    - [Les déplacements](#les-déplacements)
    - [L'envoi et la suppression des projectiles](#lenvoi-et-la-suppression-des-projectiles)
    - [Le monstre](#le-monstre)
    - [La barre de points de vie](#la-barre-de-points-de-vie)
    - [Les morts](#les-morts)
    - [Win ou Lose de la partie](#win-ou-lose-de-la-partie)
  - [Problèmes rencontrés](#problèmes-rencontrés)
    - [Latence quand il y a plusieurs clients](#latence-quand-il-y-a-plusieurs-clients)
    - [Recherche de nouveau projet](#recherche-de-nouveau-projet)
    - [Priorité et conditions](#priorité-et-conditions)
    - [Map](#map)
    - [Temps](#temps)
    
### Dépôt Git

[Lien de notre dépôt git](https://gitlab.com/antoinedelbrel/projet-dev-logiciel)

### Diaporama

[Lien de la présentation](https://www.canva.com/design/DAD9oYnnjps/2kqhKEhSFcAx3NCGz22YUA/view)

## Initialisation du projet

### Pré-requis

**Software**

- Python
- Pygame
- Sqlite
- Visual Studio Code
- Live Share

## Installation du projet

### Clonage du dépôt git

```
git clone https://gitlab.com/antoinedelbrel/projet-dev-logiciel
```

```
git clone git@gitlab.com:antoinedelbrel/projet-dev-logiciel.git
```

### Installation des dépendances

```
Avoir python3 ou plus
pip install requirement.txt --user
pip install pygames
```

### Activer le serveur

```
python server.py
```

### Lancer le menu

```
python menu.py
```

## Architecture du projet

### Images

Dans le dossier `assets` nous avons toute les images du jeu :
- la map
- les players
- les monstres
- le menu

### Le projet

Les fichiers du jeu sont situés à la racine du projet :
- `server.py` correspond au serveur du jeu
- `inputbox.py` correspond au login du jeu
- `menu.py` correspond au menu du jeu
- `pve.py`(player versus engine) lance une partie player vs bot
- `pvp.py` (player versus player) lance une partie player vs player
- `monster.py` fait bouger le monstre

## Le jeu

- Rpg en 2d avec player versus engine et player versus player.
- Serveur pour lancer le projet.
- Menu avec un login, un dictatiel pour savoir comment jouer et le lancement du jeu.

## Les Fonctionnalitées

### Le menu

- Création d'un login qui vérifie si le joueur est déjà existant sinon il créer un nouveau joueur dans la bdd
- Menu à choix multiples : Pve, Pvp, How to play
- Crud admin permettant de voir les potions stockées dans la bdd

### La Base de donnée

- Mise en place d'une bdd avec des Users et Admin
- Mise en place d'un objet potion dans la bdd

### Les déplacements

- Chaqu'une des animations sont réalisées à partir de 3 images 
- Possibilitées de se déplacer sur 8 axes différends (honrizontalement, verticalement, diagonalement)

### L'envoi et la suppression des projectiles

- Chacune des animations envoies des projectiles 
- Possibilitées de les lancer sur 8 axes différends (honrizontalement, verticalement, diagonalement)
- Suppression des projectiles lorsqu'ils touchent la hitbox du monstre ou sortent de l'écran

### Le monstre 

- Le monstre se déplace en fonction de la position du joueur 
- Le monstre attaque au niveau de la hitbox du joueur

### La barre de points de vie

- Mise en place de la barre de point vie au dessus de la hitbox du joueur ou du monstre
- La barre de point de vie évolue en fonction des dégâts subits par le joueur ou le monstre

### Les morts

- Lorsque la barre de vie est à 0pv, lancement de l'animation de mort réalisé à partir de 3 images 
- Blocage des déplacements du monstre et du joueur

### Win ou Lose de la partie

- Si le joueur tue le monstre, on affiche l'image `Player Win`
- Si le monstre tue le joueur, on affiche l'image `Monster Win`

## Problèmes rencontrés

### Latence quand il y a plusieurs clients

Au début du projet, notre objectifs etaient de faire un jeu avec plusieurs clients connécté en même temps. Il y avait une grosse latence entre chaque client donc nous avons decidé de changer de méthodes. Ensuite nous avons eu du mal à comprendre les latences sur les threads

### Recherche de nouveau projet

Nous avons eu tellement de problèmes pour rendre le jeu fluide à cause des threads et du gil python que nous avons décidé de reprendre le projet à 0. Le fait de chercher de nouvelles idées nous à fait perdre beaucoup de temps.

### Priorité et conditions

La difficultée etait de choisir le bon ordre des conditions. 

### Map

Avoir une map propre avec les contours et bloquer le personnage pour qu'il ne sorte pas de la map.

### Temps

Notre choix de faire tout les déplacements différends nous a fait perdre beaucoup de temps.


