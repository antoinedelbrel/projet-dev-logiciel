import pygame
import pickle
import socket

# Connection au serveur
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server = 'localhost'
port = 44444
addr = (server, port)
s.connect(addr)
data = s.recv(2048)

pygame.init()
arriereplan = pygame.image.load('assets/image/menu/menu.PNG')

win = pygame.display.set_mode((1062, 1035))
pressed = {}
pygame.display.set_caption("First Game")
run = True
right = False
left = False
top = False
bottom = False
space = False
lastmov = "down"
bg = pygame.image.load('assets/image/map.png')
walkCount = 0
spellCount = 0
allPositions = dict()
lastPositions = dict()
players = {}
walkCountOnline = {}
lastmovOnline = 0
walkCountOnline = 0
spellCountOnline = 0
ProjectX = 0
ProjectY = 0
nbProject = 0


class Player(pygame.sprite.Sprite):

    def __init__(self, idPlayer):
        pygame.sprite.Sprite.__init__(self)

        self.walkRight = [pygame.image.load('assets/image/player/player1Right1.png'), pygame.image.load(
            'assets/image/player/player1Right2.png'),  pygame.image.load('assets/image/player/player1Right3.png')]
        self.walkLeft = [pygame.image.load('assets/image/player/player1Left1.png'), pygame.image.load(
            'assets/image/player/player1Left2.png'), pygame.image.load('assets/image/player/player1Left3.png')]
        self.walkBottom = [pygame.image.load('assets/image/player/player1Front1.png'), pygame.image.load(
            'assets/image/player/player1Front2.png'), pygame.image.load('assets/image/player/player1Front3.png')]
        self.walkTop = [pygame.image.load('assets/image/player/player1Back1.png'), pygame.image.load(
            'assets/image/player/player1Back2.png'), pygame.image.load('assets/image/player/player1Back3.png')]

        self.walkTopRight = [pygame.image.load('assets/image/player/player1BackRight1.png'), pygame.image.load(
            'assets/image/player/player1BackRight2.png'),  pygame.image.load('assets/image/player/player1BackRight3.png')]
        self.walkTopLeft = [pygame.image.load('assets/image/player/player1BackLeft1.png'), pygame.image.load(
            'assets/image/player/player1BackLeft2.png'), pygame.image.load('assets/image/player/player1BackLeft3.png')]
        self.walkBotRight = [pygame.image.load('assets/image/player/player1FrontRight1.png'), pygame.image.load(
            'assets/image/player/player1FrontRight2.png'),  pygame.image.load('assets/image/player/player1FrontRight3.png')]
        self.walkBotLeft = [pygame.image.load('assets/image/player/player1FrontLeft1.png'), pygame.image.load(
            'assets/image/player/player1FrontLeft2.png'), pygame.image.load('assets/image/player/player1FrontLeft3.png')]

        self.spellRight = [pygame.image.load('assets/image/player/player1SpellRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellRight2.png'),  pygame.image.load('assets/image/player/player1SpellRight3.png')]
        self.spellLeft = [pygame.image.load('assets/image/player/player1SpellLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellLeft2.png'), pygame.image.load('assets/image/player/player1SpellLeft3.png')]
        self.spellBottom = [pygame.image.load('assets/image/player/player1SpellFront1.png'), pygame.image.load(
            'assets/image/player/player1SpellFront2.png'), pygame.image.load('assets/image/player/player1SpellFront3.png')]
        self.spellTop = [pygame.image.load('assets/image/player/player1SpellBack1.png'), pygame.image.load(
            'assets/image/player/player1SpellBack2.png'), pygame.image.load('assets/image/player/player1SpellBack3.png')]

        self.spellTopRight = [pygame.image.load('assets/image/player/player1SpellBackRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellBackRight2.png'),  pygame.image.load('assets/image/player/player1SpellBackRight3.png')]
        self.spellTopLeft = [pygame.image.load('assets/image/player/player1SpellBackLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellBackLeft2.png'), pygame.image.load('assets/image/player/player1SpellBackLeft3.png')]
        self.spellBotRight = [pygame.image.load('assets/image/player/player1SpellFrontRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellFrontRight2.png'),  pygame.image.load('assets/image/player/player1SpellFrontRight3.png')]
        self.spellBotLeft = [pygame.image.load('assets/image/player/player1SpellFrontLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellFrontLeft2.png'), pygame.image.load('assets/image/player/player1SpellFrontLeft3.png')]

        self.image = pygame.image.load(
            'assets/image/player/player1Front1.png')
        self.imageBot = pygame.image.load(
            'assets/image/player/player1Front1.png')
        self.imageTop = pygame.image.load(
            'assets/image/player/player1Back1.png')
        self.imageLeft = pygame.image.load(
            'assets/image/player/player1Left1.png')
        self.imageRight = pygame.image.load(
            'assets/image/player/player1Right1.png')

        self.imageTopRight = pygame.image.load(
            'assets/image/player/player1BackRight1.png')
        self.imageTopLeft = pygame.image.load(
            'assets/image/player/player1BackLeft1.png')
        self.imageBotRight = pygame.image.load(
            'assets/image/player/player1FrontRight1.png')
        self.imageBotLeft = pygame.image.load(
            'assets/image/player/player1FrontLeft1.png')

        self.id = idPlayer
        self.health = 100
        self.max_health = 100
        self.attack = 10
        self.velocity = 2.5
        self.velocityDiag = 1.25

        self.rect = self.image.get_rect()
        self.rect.x = 150
        self.rect.y = 500

        self.hitbox = (self.rect.x + 5, self.rect.y + 5, 1, 5)

        self.all_projectiles_bot = pygame.sprite.Group()
        self.all_projectiles_top = pygame.sprite.Group()
        self.all_projectiles_left = pygame.sprite.Group()
        self.all_projectiles_right = pygame.sprite.Group()
        self.all_projectiles_topRight = pygame.sprite.Group()
        self.all_projectiles_topLeft = pygame.sprite.Group()
        self.all_projectiles_botRight = pygame.sprite.Group()
        self.all_projectiles_botLeft = pygame.sprite.Group()

    def move_right(self):
        self.rect.x += self.velocity

    def move_left(self):
        self.rect.x -= self.velocity

    def move_top(self):
        self.rect.y -= self.velocity

    def move_bottom(self):
        self.rect.y += self.velocity

    def move_top_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_top_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_bot_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y += self.velocityDiag

    def move_bot_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y += self.velocityDiag

    def launch_projectile_bottom(self):
        self.all_projectiles_bot.add(ProjectileBot())

    def launch_projectile_top(self):
        self.all_projectiles_top.add(ProjectileTop())

    def launch_projectile_right(self):
        self.all_projectiles_right.add(ProjectileRight())

    def launch_projectile_left(self):
        self.all_projectiles_left.add(ProjectileLeft())

    def launch_projectile_topRight(self):
        self.all_projectiles_topRight.add(ProjectileTopRight())

    def launch_projectile_topLeft(self):
        self.all_projectiles_topLeft.add(ProjectileTopLeft())

    def launch_projectile_botRight(self):
        self.all_projectiles_botRight.add(ProjectileBotRight())

    def launch_projectile_botLeft(self):
        self.all_projectiles_botLeft.add(ProjectileBotLeft())


class ProjectileBot(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFront.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_bot(self):
        self.rect.y += self.velocity


class ProjectileTop(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBack.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_top(self):
        self.rect.y -= self.velocity


class ProjectileRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_right(self):
        self.rect.x += self.velocity


class ProjectileLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_left(self):
        self.rect.x -= self.velocity


class ProjectileTopRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBackRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_top_right(self):
        self.rect.x += self.velocity
        self.rect.y -= self.velocity


class ProjectileTopLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBackLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_top_left(self):
        self.rect.x -= self.velocity
        self.rect.y -= self.velocity


class ProjectileBotRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFrontRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_bot_right(self):
        self.rect.x += self.velocity
        self.rect.y += self.velocity


class ProjectileBotLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFrontLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    def project_move_bot_left(self):
        self.rect.x -= self.velocity
        self.rect.y += self.velocity


def traiter_donnees():
    global lastPositions, allPositions
    try:
        s.send(pickle.dumps({'pos': p.rect}))
        allPositions = pickle.loads(s.recv(2048))
    except Exception as e:
        print(str(e))
    for idPlayer, pos in allPositions.items():
        if idPlayer != s.getsockname()[1]:
            if idPlayer in players:
                print(pos)
                players[idPlayer].rect.x = pos[0]
                players[idPlayer].rect.y = pos[1]
                win.blit(players[idPlayer].image,
                         players[idPlayer].rect)
                # redrawOnlinePlayer(players[idPlayer])
                print(players[idPlayer].rect)
                pygame.display.update()
                lastPositions = allPositions
            else:
                players[idPlayer] = Player(idPlayer)
                lastPositions = allPositions


def redrawOnlinePlayer(p):
    global lastmovOnline
    global walkCountOnline
    global spellCountOnline
    global lastPositions
    global allPositions

    leftOnline = False
    rightOnline = False
    topOnline = False
    bottomOnline = False
    spaceOnline = False

    if lastPositions[p.id][0] > allPositions[p.id][0]:
        print('=== gauche')
        leftOnline = True
        rightOnline = False
        topOnline = False
        bottomOnline = False
        spaceOnline = False

    if lastPositions[p.id][0] < allPositions[p.id][0]:
        print('=== droite')
        leftOnline = False
        rightOnline = True
        topOnline = False
        bottomOnline = False
        spaceOnline = False

    if lastPositions[p.id][1] < allPositions[p.id][1]:
        leftOnline = False
        rightOnline = False
        topOnline = False
        bottomOnline = True
        spaceOnline = False

    if lastPositions[p.id][1] > allPositions[p.id][1]:
        leftOnline = False
        rightOnline = False
        topOnline = True
        bottomOnline = False
        spaceOnline = False

    if walkCountOnline + 1 >= 9:
        walkCountOnline = 0

    if spellCountOnline + 1 >= 9:
        spellCountOnline = 0
    try:
        # Annimations spell
        if space and lastmovOnline == "downRight":
            win.blit(p.spellBotRight[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "downLeft":
            win.blit(p.spellBotLeft[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "topRight":
            win.blit(p.spellTopRight[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "topLeft":
            win.blit(p.spellTopLeft[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "down":
            win.blit(p.spellBottom[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "top":
            win.blit(p.spellTop[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "left":
            win.blit(p.spellLeft[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        elif space and lastmovOnline == "right":
            win.blit(p.spellRight[spellCountOnline//3], p.rect)
            spellCountOnline += 1

        # Annimation deplacement
        elif top and right:
            win.blit(p.walkTopRight[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "topRight"

        elif top and left:
            win.blit(p.walkTopLeft[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "topLeft"

        elif bottom and right:
            win.blit(p.walkBotRight[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "downRight"

        elif bottom and left:
            win.blit(p.walkBotLeft[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "downLeft"

        elif leftOnline:
            win.blit(p.walkLeft[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "left"

        elif right:
            win.blit(p.walkRight[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "right"

        elif top:
            win.blit(p.walkTop[walkCountOnline//3], p.rect)
            walkCountOnline += 1
            lastmovOnline = "top"

        elif bottom:
            win.blit(p.walkBottom[walkCountOnline//3], )
            walkCountOnline += 1
            lastmovOnline = "down"

        # Test position
        else:
            if lastmovOnline == "down":
                win.blit(p.imageBot, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "top":
                win.blit(p.imageTop, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "left":
                win.blit(p.imageLeft, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "right":
                win.blit(p.imageRight, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "topRight":
                win.blit(p.imageTopRight, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "topLeft":
                win.blit(p.imageTopLeft, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "downRight":
                win.blit(p.imageBotRight, p.rect)
                walkCountOnline = 0

            if lastmovOnline == "downLeft":
                win.blit(p.imageBotLeft, p.rect)
                walkCountOnline = 0
    except Exception as e:
        print(str(e))
    pygame.draw.rect(win, (0, 128, 0),
                     (p.hitbox[0], p.hitbox[1]-20, p.health/2, 10))
    # pygame.draw.rect(win, (0,128,0), (p.hitbox[0], p.hitbox[1]-20, (5 * (10 - p.health)), 10 ))
    p.hitbox = (p.rect.x, p.rect.y + 2, 30, 57)


def redrawGameWindow(p):
    global lastmov
    global walkCount
    global spellCount
    global ProjectY
    global ProjectX

    if walkCount + 1 >= 9:
        walkCount = 0

    if spellCount + 1 >= 9:
        spellCount = 0

    if space and lastmov == "downRight":
        win.blit(p.spellBotRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = 20
            p.launch_projectile_botRight()

    elif space and lastmov == "downLeft":
        win.blit(p.spellBotLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = 20
            p.launch_projectile_botLeft()

    elif space and lastmov == "topRight":
        win.blit(p.spellTopRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = -20
            p.launch_projectile_topRight()

    elif space and lastmov == "topLeft":
        win.blit(p.spellTopLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = -20
            p.launch_projectile_topLeft()

    elif space and lastmov == "down":
        win.blit(p.spellBottom[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 5
            ProjectY = 20
            p.launch_projectile_bottom()

    elif space and lastmov == "top":
        win.blit(p.spellTop[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 5
            ProjectY = -20
            p.launch_projectile_top()

    elif space and lastmov == "left":
        win.blit(p.spellLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = 5
            p.launch_projectile_left()

    elif space and lastmov == "right":
        win.blit(p.spellRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = 5
            p.launch_projectile_right()

    elif top and right:
        win.blit(p.walkTopRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "topRight"

    elif top and left:
        win.blit(p.walkTopLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "topLeft"

    elif bottom and right:
        win.blit(p.walkBotRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "downRight"

    elif bottom and left:
        win.blit(p.walkBotLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "downLeft"

    elif left:
        win.blit(p.walkLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "left"

    elif right:
        win.blit(p.walkRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "right"

    elif top:
        win.blit(p.walkTop[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "top"

    elif bottom:
        win.blit(p.walkBottom[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "down"

    else:
        if lastmov == "down":
            win.blit(p.imageBot, p.rect)
            walkCount = 0

        elif lastmov == "top":
            win.blit(p.imageTop, p.rect)
            walkCount = 0

        elif lastmov == "left":
            win.blit(p.imageLeft, p.rect)
            walkCount = 0

        elif lastmov == "right":
            win.blit(p.imageRight, p.rect)
            walkCount = 0

        elif lastmov == "topRight":
            win.blit(p.imageTopRight, p.rect)
            walkCount = 0

        elif lastmov == "topLeft":
            win.blit(p.imageTopLeft, p.rect)
            walkCount = 0

        elif lastmov == "downRight":
            win.blit(p.imageBotRight, p.rect)
            walkCount = 0

        elif lastmov == "downLeft":
            win.blit(p.imageBotLeft, p.rect)
            walkCount = 0

    pygame.draw.rect(win, (0, 128, 0),
                     (p.hitbox[0], p.hitbox[1]-20, p.health/2, 10))

    # pygame.draw.rect(win, (0,128,0), (p.hitbox[0], p.hitbox[1]-20, (5 * (10 - p.health)), 10 ))

    p.hitbox = (p.rect.x, p.rect.y + 2, 30, 57)

    for ProjectileBot in p.all_projectiles_bot:
        ProjectileBot.project_move_bot()

    for ProjectileTop in p.all_projectiles_top:
        ProjectileTop.project_move_top()

    for ProjectileRight in p.all_projectiles_right:
        ProjectileRight.project_move_right()

    for ProjectileLeft in p.all_projectiles_left:
        ProjectileLeft.project_move_left()

    for ProjectileBotRight in p.all_projectiles_botRight:
        ProjectileBotRight.project_move_bot_right()

    for ProjectileBotLeft in p.all_projectiles_botLeft:
        ProjectileBotLeft.project_move_bot_left()

    for ProjectileTopRight in p.all_projectiles_topRight:
        ProjectileTopRight.project_move_top_right()

    for ProjectileTopLeft in p.all_projectiles_topLeft:
        ProjectileTopLeft.project_move_top_left()

    p.all_projectiles_bot.draw(win)
    p.all_projectiles_top.draw(win)
    p.all_projectiles_left.draw(win)
    p.all_projectiles_right.draw(win)
    p.all_projectiles_topRight.draw(win)
    p.all_projectiles_topLeft.draw(win)
    p.all_projectiles_botRight.draw(win)
    p.all_projectiles_botLeft.draw(win)

    pygame.display.update()


p = Player(s.getsockname()[1])

while run:
    # print(pressed)

    traiter_donnees()

    win.blit(bg, (0, 0))

    if pressed.get(32):
        print('SPACE')
        right = False
        left = False
        top = False
        bottom = False
        space = True

    elif pressed.get(100) and pressed.get(122) and p.rect.y > 19 and p.rect.x < 1011:
        print('RIGHT TOP')
        right = True
        left = False
        top = True
        bottom = False
        space = False
        p.move_top_right()

    elif pressed.get(113) and pressed.get(122) and p.rect.y > 19 and p.rect.x > 0:
        print('LEFT TOP')
        right = False
        left = True
        top = True
        bottom = False
        space = False
        p.move_top_left()

    elif pressed.get(100) and pressed.get(115) and p.rect.y < 983 and p.rect.x < 1011:
        print('RIGHT BOT')
        right = True
        left = False
        top = False
        bottom = True
        space = False
        p.move_bot_right()

    elif pressed.get(113) and pressed.get(115) and p.rect.x > 0 and p.rect.y < 983:
        print('LEFT BOT')
        right = False
        left = True
        top = False
        bottom = True
        space = False
        p.move_bot_left()

    elif pressed.get(122) and p.rect.y > 19:  # 119 en AZERTY - 122 en QWERTY
        left = False
        right = False
        top = True
        bottom = False
        space = False
        print('UP')
        p.move_top()

    elif pressed.get(113) and p.rect.x > 0:  # 97 en AZERTY - 113 en QWERTY
        print('LEFT')
        left = True
        right = False
        top = False
        bottom == False
        space = False
        p.move_left()

    elif pressed.get(115) and p.rect.y < 983:
        left = False
        right = False
        top = False
        bottom = True
        space = False
        print('DOWN')
        p.move_bottom()

    elif pressed.get(100) and p.rect.x < 1011:
        print('RIGHT')
        right = True
        left = False
        top = False
        bottom = False
        space = False
        p.move_right()

    else:
        right = False
        left = False
        top = False
        bottom = False
        space = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()

        elif event.type == pygame.KEYDOWN:
            pressed[event.key] = True

        elif event.type == pygame.KEYUP:
            pressed[event.key] = False

    redrawGameWindow(p)

pygame.quit()
