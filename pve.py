import pygame
import pickle
import socket


pygame.init()

# Variables
arriereplan = pygame.image.load('assets/image/menu/menu.PNG')
font = pygame.font.SysFont(None, 50)
win = pygame.display.set_mode((1062, 1035))
pressed = {}
run = True
right = False
left = False
top = False
bottom = False
space = False
lastmov = "down"
bg = pygame.image.load('assets/image/map.png')
walkCount = 0
spellCount = 0
deathCount = 0
players = {}
ProjectX = 0
ProjectY = 0
nbProject = 0


class Player(pygame.sprite.Sprite):

    def __init__(self, idPlayer):
        pygame.sprite.Sprite.__init__(self)

        # Images utilisées pour les animations de déplacement du joueurs
        self.walkRight = [pygame.image.load('assets/image/player/player1Right1.png'), pygame.image.load(
            'assets/image/player/player1Right2.png'),  pygame.image.load('assets/image/player/player1Right3.png')]
        self.walkLeft = [pygame.image.load('assets/image/player/player1Left1.png'), pygame.image.load(
            'assets/image/player/player1Left2.png'), pygame.image.load('assets/image/player/player1Left3.png')]
        self.walkBottom = [pygame.image.load('assets/image/player/player1Front1.png'), pygame.image.load(
            'assets/image/player/player1Front2.png'), pygame.image.load('assets/image/player/player1Front3.png')]
        self.walkTop = [pygame.image.load('assets/image/player/player1Back1.png'), pygame.image.load(
            'assets/image/player/player1Back2.png'), pygame.image.load('assets/image/player/player1Back3.png')]
        self.walkTopRight = [pygame.image.load('assets/image/player/player1BackRight1.png'), pygame.image.load(
            'assets/image/player/player1BackRight2.png'),  pygame.image.load('assets/image/player/player1BackRight3.png')]
        self.walkTopLeft = [pygame.image.load('assets/image/player/player1BackLeft1.png'), pygame.image.load(
            'assets/image/player/player1BackLeft2.png'), pygame.image.load('assets/image/player/player1BackLeft3.png')]
        self.walkBotRight = [pygame.image.load('assets/image/player/player1FrontRight1.png'), pygame.image.load(
            'assets/image/player/player1FrontRight2.png'),  pygame.image.load('assets/image/player/player1FrontRight3.png')]
        self.walkBotLeft = [pygame.image.load('assets/image/player/player1FrontLeft1.png'), pygame.image.load(
            'assets/image/player/player1FrontLeft2.png'), pygame.image.load('assets/image/player/player1FrontLeft3.png')]

        # Images utilisées pour les animations de sort du joueurs
        self.spellRight = [pygame.image.load('assets/image/player/player1SpellRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellRight2.png'),  pygame.image.load('assets/image/player/player1SpellRight3.png')]
        self.spellLeft = [pygame.image.load('assets/image/player/player1SpellLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellLeft2.png'), pygame.image.load('assets/image/player/player1SpellLeft3.png')]
        self.spellBottom = [pygame.image.load('assets/image/player/player1SpellFront1.png'), pygame.image.load(
            'assets/image/player/player1SpellFront2.png'), pygame.image.load('assets/image/player/player1SpellFront3.png')]
        self.spellTop = [pygame.image.load('assets/image/player/player1SpellBack1.png'), pygame.image.load(
            'assets/image/player/player1SpellBack2.png'), pygame.image.load('assets/image/player/player1SpellBack3.png')]
        self.spellTopRight = [pygame.image.load('assets/image/player/player1SpellBackRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellBackRight2.png'),  pygame.image.load('assets/image/player/player1SpellBackRight3.png')]
        self.spellTopLeft = [pygame.image.load('assets/image/player/player1SpellBackLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellBackLeft2.png'), pygame.image.load('assets/image/player/player1SpellBackLeft3.png')]
        self.spellBotRight = [pygame.image.load('assets/image/player/player1SpellFrontRight1.png'), pygame.image.load(
            'assets/image/player/player1SpellFrontRight2.png'),  pygame.image.load('assets/image/player/player1SpellFrontRight3.png')]
        self.spellBotLeft = [pygame.image.load('assets/image/player/player1SpellFrontLeft1.png'), pygame.image.load(
            'assets/image/player/player1SpellFrontLeft2.png'), pygame.image.load('assets/image/player/player1SpellFrontLeft3.png')]

        # Images utilisées pour les animations de mort du joueurs
        self.death = [pygame.image.load('assets/image/player/player1Death1.png'), pygame.image.load(
            'assets/image/player/player1Death2.png'), pygame.image.load('assets/image/player/player1Death3.png')]

        # Images utilisées pour les moments statiques du joueurs
        self.image = pygame.image.load(
            'assets/image/player/player1Front1.png')
        self.imageBot = pygame.image.load(
            'assets/image/player/player1Front1.png')
        self.imageTop = pygame.image.load(
            'assets/image/player/player1Back1.png')
        self.imageLeft = pygame.image.load(
            'assets/image/player/player1Left1.png')
        self.imageRight = pygame.image.load(
            'assets/image/player/player1Right1.png')
        self.imageDeath = pygame.image.load(
            'assets/image/player/player1Death3.png')
        self.imageTopRight = pygame.image.load(
            'assets/image/player/player1BackRight1.png')
        self.imageTopLeft = pygame.image.load(
            'assets/image/player/player1BackLeft1.png')
        self.imageBotRight = pygame.image.load(
            'assets/image/player/player1FrontRight1.png')
        self.imageBotLeft = pygame.image.load(
            'assets/image/player/player1FrontLeft1.png')

        # Caractéristique du joueur
        self.id = idPlayer
        self.health = 100
        self.max_health = 100
        self.attack = 2
        self.velocity = 2.5
        self.velocityDiag = 2
        self.rect = self.image.get_rect()
        self.rect.x = 300
        self.rect.y = 500
        self.hitbox = (self.rect.x, self.rect.y, 40, 50)

        # Création de groupes stockant les projectiles
        self.all_projectiles_bot = pygame.sprite.Group()
        self.all_projectiles_top = pygame.sprite.Group()
        self.all_projectiles_left = pygame.sprite.Group()
        self.all_projectiles_right = pygame.sprite.Group()
        self.all_projectiles_topRight = pygame.sprite.Group()
        self.all_projectiles_topLeft = pygame.sprite.Group()
        self.all_projectiles_botRight = pygame.sprite.Group()
        self.all_projectiles_botLeft = pygame.sprite.Group()

    # Toutes les fonctions de déplacement du joueur
    def move_right(self):
        self.rect.x += self.velocity

    def move_left(self):
        self.rect.x -= self.velocity

    def move_top(self):
        self.rect.y -= self.velocity

    def move_bottom(self):
        self.rect.y += self.velocity

    def move_top_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_top_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_bot_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y += self.velocityDiag

    def move_bot_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y += self.velocityDiag

    # Toutes les fonctions de lancement de projectile
    def launch_projectile_bottom(self):
        self.all_projectiles_bot.add(ProjectileBot())

    def launch_projectile_top(self):
        self.all_projectiles_top.add(ProjectileTop())

    def launch_projectile_right(self):
        self.all_projectiles_right.add(ProjectileRight())

    def launch_projectile_left(self):
        self.all_projectiles_left.add(ProjectileLeft())

    def launch_projectile_topRight(self):
        self.all_projectiles_topRight.add(ProjectileTopRight())

    def launch_projectile_topLeft(self):
        self.all_projectiles_topLeft.add(ProjectileTopLeft())

    def launch_projectile_botRight(self):
        self.all_projectiles_botRight.add(ProjectileBotRight())

    def launch_projectile_botLeft(self):
        self.all_projectiles_botLeft.add(ProjectileBotLeft())

    # Fonction pour vérifier si il y a une collision
    def check_colision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

    # Fonction de décrémentation de la vie du monstre
    def attack_by_player(self, monster):
        if monster.health > 0:
            monster.health -= self.attack
        monster.max_health -= self.attack

# Toutes les classes projectile, une classe par direction


class ProjectileBot(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFront.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_bot(self):
        self.rect.y += self.velocity


class ProjectileTop(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBack.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_top(self):
        self.rect.y -= self.velocity


class ProjectileRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_right(self):
        self.rect.x += self.velocity


class ProjectileLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 5
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_left(self):
        self.rect.x -= self.velocity


class ProjectileTopRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBackRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_top_right(self):
        self.rect.x += self.velocity
        self.rect.y -= self.velocity


class ProjectileTopLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileBackLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_top_left(self):
        self.rect.x -= self.velocity
        self.rect.y -= self.velocity


class ProjectileBotRight(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFrontRight.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_bot_right(self):
        self.rect.x += self.velocity
        self.rect.y += self.velocity


class ProjectileBotLeft(pygame.sprite.Sprite):
    global ProjectX
    global ProjectY

    # Caractéristique du projectile
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load(
            "assets/image/player/player1ProjectileFrontLeft.png")
        self.rect = self.image.get_rect()
        self.rect.x = p.rect.x + ProjectX
        self.rect.y = p.rect.y + ProjectY

    # Fonction de déplacement du projectile
    def project_move_bot_left(self):
        self.rect.x -= self.velocity
        self.rect.y += self.velocity


class Monster(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        # Chargement des images d'animation de déplacement du monstre
        self.walkRight = [pygame.image.load('assets/image/monster/monster1Right1.png'), pygame.image.load(
            'assets/image/monster/monster1Right2.png'),  pygame.image.load('assets/image/monster/monster1Right3.png')]
        self.walkLeft = [pygame.image.load('assets/image/monster/monster1Left1.png'), pygame.image.load(
            'assets/image/monster/monster1Left2.png'), pygame.image.load('assets/image/monster/monster1Left3.png')]
        self.walkBottom = [pygame.image.load('assets/image/monster/monster1Front1.png'), pygame.image.load(
            'assets/image/monster/monster1Front2.png'), pygame.image.load('assets/image/monster/monster1Front3.png')]
        self.walkTop = [pygame.image.load('assets/image/monster/monster1Back1.png'), pygame.image.load(
            'assets/image/monster/monster1Back2.png'), pygame.image.load('assets/image/monster/monster1Back3.png')]
        self.walkTopRight = [pygame.image.load('assets/image/monster/monster1BackRight1.png'), pygame.image.load(
            'assets/image/monster/monster1BackRight2.png'),  pygame.image.load('assets/image/monster/monster1BackRight3.png')]
        self.walkTopLeft = [pygame.image.load('assets/image/monster/monster1BackLeft1.png'), pygame.image.load(
            'assets/image/monster/monster1BackLeft2.png'), pygame.image.load('assets/image/monster/monster1BackLeft3.png')]
        self.walkBotRight = [pygame.image.load('assets/image/monster/monster1FrontRight1.png'), pygame.image.load(
            'assets/image/monster/monster1FrontRight2.png'),  pygame.image.load('assets/image/monster/monster1FrontRight3.png')]
        self.walkBotLeft = [pygame.image.load('assets/image/monster/monster1FrontLeft1.png'), pygame.image.load(
            'assets/image/monster/monster1FrontLeft2.png'), pygame.image.load('assets/image/monster/monster1FrontLeft3.png')]

        # Chargement des images d'animation d'attaque du monstre
        self.spellRight = [pygame.image.load('assets/image/monster/monster1SpellRight1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellRight2.png'),  pygame.image.load('assets/image/monster/monster1SpellRight3.png')]
        self.spellLeft = [pygame.image.load('assets/image/monster/monster1SpellLeft1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellLeft2.png'), pygame.image.load('assets/image/monster/monster1SpellLeft3.png')]
        self.spellBottom = [pygame.image.load('assets/image/monster/monster1SpellFront1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellFront2.png'), pygame.image.load('assets/image/monster/monster1SpellFront3.png')]
        self.spellTop = [pygame.image.load('assets/image/monster/monster1SpellBack1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellBack2.png'), pygame.image.load('assets/image/monster/monster1SpellBack3.png')]
        self.spellTopRight = [pygame.image.load('assets/image/monster/monster1SpellBackRight1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellBackRight2.png'),  pygame.image.load('assets/image/monster/monster1SpellBackRight3.png')]
        self.spellTopLeft = [pygame.image.load('assets/image/monster/monster1SpellBackLeft1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellBackLeft2.png'), pygame.image.load('assets/image/monster/monster1SpellBackLeft3.png')]
        self.spellBotRight = [pygame.image.load('assets/image/monster/monster1SpellFrontRight1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellFrontRight2.png'),  pygame.image.load('assets/image/monster/monster1SpellFrontRight3.png')]
        self.spellBotLeft = [pygame.image.load('assets/image/monster/monster1SpellFrontLeft1.png'), pygame.image.load(
            'assets/image/monster/monster1SpellFrontLeft2.png'), pygame.image.load('assets/image/monster/monster1SpellFrontLeft3.png')]

        # Chargement des images d'animation de mort du monstre
        self.death = [pygame.image.load('assets/image/monster/monster1Death1.png'), pygame.image.load(
            'assets/image/monster/monster1Death1.png'), pygame.image.load('assets/image/monster/monster1Death1.png')]
        self.imageDeath = pygame.image.load(
            'assets/image/monster/monster1Death1.png')

        # Chargement des images statique du monstre
        self.image = pygame.image.load(
            'assets/image/monster/monster1Front1.png')
        self.imageBot = pygame.image.load(
            'assets/image/monster/monster1Front1.png')
        self.imageTop = pygame.image.load(
            'assets/image/monster/monster1Back1.png')
        self.imageLeft = pygame.image.load(
            'assets/image/monster/monster1Left1.png')
        self.imageRight = pygame.image.load(
            'assets/image/monster/monster1Right1.png')
        self.imageTopRight = pygame.image.load(
            'assets/image/monster/monster1BackRight1.png')
        self.imageTopLeft = pygame.image.load(
            'assets/image/monster/monster1BackLeft1.png')
        self.imageBotRight = pygame.image.load(
            'assets/image/monster/monster1FrontRight1.png')
        self.imageBotLeft = pygame.image.load(
            'assets/image/monster/monster1FrontLeft1.png')

        # Variable du monstre
        self.health = 100
        self.max_health = 100
        self.attack = 5
        self.velocity = 2
        self.velocityDiag = 1.5
        self.walkCount = 0
        self.lastmov = "down"
        self.spellCount = 0
        self.rect = self.image.get_rect()
        self.rect.x = 700
        self.rect.y = 500
        self.deathCount = 0
        self.hitbox = (self.rect.x, self.rect.y, 40, 50)

    # Fonction qui vérifie si il y a une collision
    def check_colision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

    # Fonction de déplacement
    def move_right(self):
        self.rect.x += self.velocity

    def move_left(self):
        self.rect.x -= self.velocity

    def move_top(self):
        self.rect.y -= self.velocity

    def move_bottom(self):
        self.rect.y += self.velocity

    def move_top_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_top_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y -= self.velocityDiag

    def move_bot_right(self):
        self.rect.x += self.velocityDiag
        self.rect.y += self.velocityDiag

    def move_bot_left(self):
        self.rect.x -= self.velocityDiag
        self.rect.y += self.velocityDiag

    def attack_by_monster(self, player):
        if player.health > 0:
            player.health -= self.attack
        player.max_health -= self.attack

    # Fonction principale du monstre
    def move_on_player(self):

        # Si le monstre meurt
        if self.max_health <= 0 and self.deathCount <= 8:
            win.blit(self.death[self.deathCount//3], self.rect)
            if self.deathCount < 8:
                self.deathCount += 1
            if self.deathCount == 8:
                win.blit(self.imageDeath, self.rect)
                end('Player')

        if self.walkCount + 1 >= 9:
            self.walkCount = 0

        if self.spellCount + 1 >= 9:
            self.spellCount = 0

        # Logique du monstre / affiche l'image du sprite selon l'action éffectué
        if p.max_health < 0 or self.health < 8:
            if self.lastmov == "downRight":
                win.blit(self.imageBotRight, self.rect)

            elif self.lastmov == "downleft":
                win.blit(self.imageBotLeft, self.rect)

            elif self.lastmov == "topRight":
                win.blit(self.imageTopRight, self.rect)

            elif self.lastmov == "topLeft":
                win.blit(self.imageTopLeft, self.rect)

            elif self.lastmov == "right":
                win.blit(self.imageRight, self.rect)

            elif self.lastmov == "left":
                win.blit(self.imageLeft, self.rect)

            elif self.lastmov == "down":
                win.blit(self.imageBot, self.rect)

            elif self.lastmov == "top":
                win.blit(self.imageTop, self.rect)

        # Test des collisions / Si oui Attaque
        elif [p] == self.check_colision(self, [p]) and self.lastmov == "downRight":
            win.blit(self.spellBotRight[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "downleft":
            win.blit(self.spellBotLeft[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "topRight":
            win.blit(self.spellTopRight[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "topLeft":
            win.blit(self.spellTopLeft[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "right":
            win.blit(self.spellRight[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "left":
            win.blit(self.spellLeft[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "down":
            win.blit(self.spellBottom[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        elif [p] == self.check_colision(self, [p]) and self.lastmov == "top":
            win.blit(self.spellTop[self.spellCount//3], self.rect)
            self.spellCount += 1
            if self.spellCount == 3:
                self.attack_by_monster(p)

        # Intelligence du Bot / Se dirige vers le Joueur en fonction de sa poistion
        elif p.rect.x < self.rect.x and p.rect.y < self.rect.y:
            win.blit(self.walkTopLeft[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "topLeft"
            self.move_top_left()

        elif p.rect.x > self.rect.x and p.rect.y > self.rect.y:
            win.blit(self.walkBotRight[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "downRight"
            self.move_bot_right()

        elif p.rect.x < self.rect.x and p.rect.y > self.rect.y:
            win.blit(self.walkBotLeft[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "downLeft"
            self.move_bot_left()

        elif p.rect.x > self.rect.x and p.rect.y < self.rect.y:
            win.blit(self.walkTopRight[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "topRight"
            self.move_top_right()

        elif p.rect.x == self.rect.x and p.rect.y < self.rect.y:
            win.blit(self.walkTop[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "top"
            self.move_top()

        elif p.rect.x == self.rect.x and p.rect.y > self.rect.y:
            win.blit(self.walkBottom[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "down"
            self.move_bottom()

        elif p.rect.x < self.rect.x and p.rect.y == self.rect.y:
            win.blit(self.walkLeft[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "left"
            self.move_left()

        elif p.rect.x > self.rect.x and p.rect.y == self.rect.y:
            win.blit(self.walkRight[self.walkCount//3], self.rect)
            self.walkCount += 1
            self.lastmov = "right"
            self.move_right()

        # Logique affichage de la direction quand le monstre est statique
        else:
            if lastmov == "down":
                win.blit(self.imageBot, self.rect)
                self.walkCount = 0

            elif lastmov == "top":
                win.blit(self.imageTop, self.rect)
                self.walkCount = 0

            elif lastmov == "left":
                win.blit(self.imageLeft, self.rect)
                self.walkCount = 0

            elif lastmov == "right":
                win.blit(self.imageRight, self.rect)
                self.walkCount = 0

            elif lastmov == "topRight":
                win.blit(self.imageTopRight, self.rect)
                self.walkCount = 0

            elif lastmov == "topLeft":
                win.blit(self.imageTopLeft, self.rect)
                self.walkCount = 0

            elif lastmov == "downRight":
                win.blit(self.imageBotRight, self.rect)
                self.walkCount = 0

            elif lastmov == "downLeft":
                win.blit(self.imageBotLeft, self.rect)
                self.walkCount = 0

        # Affichage de la barre de vie et mise en place de la hitbox
        pygame.draw.rect(win, (0, 128, 0),
                         (self.hitbox[0], self.hitbox[1]-20, self.health/2, 10))
        self.hitbox = (self.rect.x, self.rect.y, 40, 50)

    pygame.display.update()

# Fonction principale du Joueur


def redrawGameWindow(p):
    global lastmov
    global walkCount
    global spellCount
    global deathCount
    global ProjectY
    global ProjectX

    if walkCount + 1 >= 9:
        walkCount = 0

    if spellCount + 1 >= 9:
        spellCount = 0

    # Si le Joeurs meurt
    if p.max_health < 0 and deathCount <= 8:
        win.blit(p.death[deathCount//3], p.rect)
        if deathCount < 8:
            deathCount += 1
        if deathCount == 8:
            win.blit(p.imageDeath, p.rect)
            # Ecran de fin
            end('Monster')

    # Si le joueur attaque / verifie la position pour afficher l'image
    elif space and lastmov == "downRight":
        win.blit(p.spellBotRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = 20
            p.launch_projectile_botRight()

    elif space and lastmov == "downLeft":
        win.blit(p.spellBotLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = 20
            p.launch_projectile_botLeft()

    elif space and lastmov == "topRight":
        win.blit(p.spellTopRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = -20
            p.launch_projectile_topRight()

    elif space and lastmov == "topLeft":
        win.blit(p.spellTopLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = -20
            p.launch_projectile_topLeft()

    elif space and lastmov == "down":
        win.blit(p.spellBottom[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 5
            ProjectY = 20
            p.launch_projectile_bottom()

    elif space and lastmov == "top":
        win.blit(p.spellTop[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 5
            ProjectY = -20
            p.launch_projectile_top()

    elif space and lastmov == "left":
        win.blit(p.spellLeft[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = -20
            ProjectY = 5
            p.launch_projectile_left()

    elif space and lastmov == "right":
        win.blit(p.spellRight[spellCount//3], p.rect)
        spellCount += 1
        if spellCount == 3:
            ProjectX = 20
            ProjectY = 5
            p.launch_projectile_right()

    # Vérifie l'action précedente (pour pouvoir afficher les bon sprite..)
    elif top and right:
        win.blit(p.walkTopRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "topRight"

    elif top and left:
        win.blit(p.walkTopLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "topLeft"

    elif bottom and right:
        win.blit(p.walkBotRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "downRight"

    elif bottom and left:
        win.blit(p.walkBotLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "downLeft"

    elif left:
        win.blit(p.walkLeft[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "left"

    elif right:
        win.blit(p.walkRight[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "right"

    elif top:
        win.blit(p.walkTop[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "top"

    elif bottom:
        win.blit(p.walkBottom[walkCount//3], p.rect)
        walkCount += 1
        lastmov = "down"

    # Affichage des images lorsque le joueur est statique
    else:

        if lastmov == "down":
            win.blit(p.imageBot, p.rect)
            walkCount = 0

        elif lastmov == "top":
            win.blit(p.imageTop, p.rect)
            walkCount = 0

        elif lastmov == "left":
            win.blit(p.imageLeft, p.rect)
            walkCount = 0

        elif lastmov == "right":
            win.blit(p.imageRight, p.rect)
            walkCount = 0

        elif lastmov == "topRight":
            win.blit(p.imageTopRight, p.rect)
            walkCount = 0

        elif lastmov == "topLeft":
            win.blit(p.imageTopLeft, p.rect)
            walkCount = 0

        elif lastmov == "downRight":
            win.blit(p.imageBotRight, p.rect)
            walkCount = 0

        elif lastmov == "downLeft":
            win.blit(p.imageBotLeft, p.rect)
            walkCount = 0

    # Mise en place de la barre de vie et de la hitbox
    pygame.draw.rect(win, (0, 128, 0),
                     (p.hitbox[0], p.hitbox[1]-20, p.health/2, 10))

    p.hitbox = (p.rect.x, p.rect.y, 40, 50)

    # Déplacement des projectiles envoyé et suppression de ces derniers si ils sortent de l'écran ou rencontre une hitbox
    for ProjectileBot in p.all_projectiles_bot:
        ProjectileBot.project_move_bot()
        if ProjectileBot.rect.y > 1035:
            p.all_projectiles_bot.remove(ProjectileBot)

        elif [m] == p.check_colision(ProjectileBot, [m]):
            p.attack_by_player(m)
            p.all_projectiles_bot.remove(ProjectileBot)

    for ProjectileTop in p.all_projectiles_top:
        ProjectileTop.project_move_top()
        if ProjectileTop.rect.y < 0:
            p.all_projectiles_top.remove(ProjectileTop)

        elif [m] == p.check_colision(ProjectileTop, [m]):
            p.attack_by_player(m)
            p.all_projectiles_top.remove(ProjectileTop)

    for ProjectileRight in p.all_projectiles_right:
        ProjectileRight.project_move_right()
        if ProjectileRight.rect.x > 1062:
            p.all_projectiles_right.remove(ProjectileRight)

        elif [m] == p.check_colision(ProjectileRight, [m]):
            p.attack_by_player(m)
            p.all_projectiles_right.remove(ProjectileRight)

    for ProjectileLeft in p.all_projectiles_left:
        ProjectileLeft.project_move_left()
        if ProjectileLeft.rect.x < 0:
            p.all_projectiles_left.remove(ProjectileLeft)

        elif [m] == p.check_colision(ProjectileLeft, [m]):
            p.attack_by_player(m)
            p.all_projectiles_left.remove(ProjectileLeft)

    for ProjectileBotRight in p.all_projectiles_botRight:
        ProjectileBotRight.project_move_bot_right()
        if ProjectileBotRight.rect.y > 1035 or ProjectileBotRight.rect.x > 1062:
            p.all_projectiles_botRight.remove(ProjectileBotRight)

        elif [m] == p.check_colision(ProjectileBotRight, [m]):
            p.attack_by_player(m)
            p.all_projectiles_botRight.remove(ProjectileBotRight)

    for ProjectileBotLeft in p.all_projectiles_botLeft:
        ProjectileBotLeft.project_move_bot_left()
        if ProjectileBotLeft.rect.y > 1035 or ProjectileBotLeft.rect.x < 0:
            p.all_projectiles_botLeft.remove(ProjectileBotLeft)

        elif [m] == p.check_colision(ProjectileBotLeft, [m]):
            p.attack_by_player(m)
            p.all_projectiles_botLeft.remove(ProjectileBotLeft)

    for ProjectileTopRight in p.all_projectiles_topRight:
        ProjectileTopRight.project_move_top_right()
        if ProjectileTopRight.rect.y < 0 or ProjectileTopRight.rect.x > 1062:
            p.all_projectiles_topRight.remove(ProjectileTopRight)

        elif [m] == p.check_colision(ProjectileTopRight, [m]):
            p.attack_by_player(m)
            p.all_projectiles_topRight.remove(ProjectileTopRight)

    for ProjectileTopLeft in p.all_projectiles_topLeft:
        ProjectileTopLeft.project_move_top_left()
        if ProjectileTopLeft.rect.y < 0 or ProjectileTopLeft.rect.x < 0:
            p.all_projectiles_topLeft.remove(ProjectileTopLeft)

        elif [m] == p.check_colision(ProjectileTopLeft, [m]):
            p.attack_by_player(m)
            p.all_projectiles_topLeft.remove(ProjectileTopLeft)

    # Affichage des projectiles lancés
    p.all_projectiles_bot.draw(win)
    p.all_projectiles_top.draw(win)
    p.all_projectiles_left.draw(win)
    p.all_projectiles_right.draw(win)
    p.all_projectiles_topRight.draw(win)
    p.all_projectiles_topLeft.draw(win)
    p.all_projectiles_botRight.draw(win)
    p.all_projectiles_botLeft.draw(win)

    pygame.display.update()

# Fonction qui simplifie la creation de text


def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, 1, color)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)

# Fonction / Ecran de fin


def end(winner):
    while True:
        draw_text(f'{winner} Win', font, (255, 255, 255), win, 400, 400)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
        pygame.display.update()


# Création du joueur et du monstre
p = Player(0)
m = Monster()

while run:
    win.blit(bg, (0, 0))

    # Vérifier si le joueur ou le monstre à encore des points de vue pour bloquer les déplacements du joueurs
    if p.max_health <= 0 or m.max_health <= 0:
        right = False
        left = False
        top = False
        bottom = False
        space = False

    # Vérifier si la barre espace est appuyé
    elif pressed.get(32):
        right = False
        left = False
        top = False
        bottom = False
        space = True

    # Vérifier si la touche D et Z sont appuyées et que le joueur ne dépasse pas de la map sur cette diagonale
    elif pressed.get(100) and pressed.get(122) and p.rect.y > 19 and p.rect.x < 1011:
        right = True
        left = False
        top = True
        bottom = False
        space = False
        p.move_top_right()

    # Vérifier si la touche Q et Z sont appuyées et que le joueur ne dépasse pas de la map sur cette diagonale
    elif pressed.get(113) and pressed.get(122) and p.rect.y > 19 and p.rect.x > 0:
        right = False
        left = True
        top = True
        bottom = False
        space = False
        p.move_top_left()

    # Vérifier si la touche D et S sont appuyées et que le joueur ne dépasse pas de la map sur cette diagonale
    elif pressed.get(100) and pressed.get(115) and p.rect.y < 983 and p.rect.x < 1011:
        right = True
        left = False
        top = False
        bottom = True
        space = False
        p.move_bot_right()

    # Vérifier si la touche Q et S sont appuyées et que le joueur ne dépasse pas de la map sur cette diagonale
    elif pressed.get(113) and pressed.get(115) and p.rect.x > 0 and p.rect.y < 983:
        right = False
        left = True
        top = False
        bottom = True
        space = False
        p.move_bot_left()

    # Vérifier si la touche Z est appuyé et que le joueur ne dépasse pas de la map sur cet axe
    elif pressed.get(122) and p.rect.y > 19:  # 119 en AZERTY - 122 en QWERTY
        left = False
        right = False
        top = True
        bottom = False
        space = False
        p.move_top()

    # Vérifier si la touche Q est appuyé et que le joueur ne dépasse pas de la map sur cet axe
    elif pressed.get(113) and p.rect.x > 0:  # 97 en AZERTY - 113 en QWERTY
        left = True
        right = False
        top = False
        bottom == False
        space = False
        p.move_left()

    # Vérifier si la touche S est appuyé et que le joueur ne dépasse pas de la map sur cet axe
    elif pressed.get(115) and p.rect.y < 983:
        left = False
        right = False
        top = False
        bottom = True
        space = False
        p.move_bottom()

    # Vérifier si la touche ZDest appuyé et que le joueur ne dépasse pas de la map sur cet axe
    elif pressed.get(100) and p.rect.x < 1011:
        right = True
        left = False
        top = False
        bottom = False
        space = False
        p.move_right()

    # Sinon aucun touche à vrai
    else:
        right = False
        left = False
        top = False
        bottom = False
        space = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()

        elif event.type == pygame.KEYDOWN:
            pressed[event.key] = True

        elif event.type == pygame.KEYUP:
            pressed[event.key] = False

    m.move_on_player()
    redrawGameWindow(p)

pygame.quit()
