from pygame.locals import *
import pygame
import sys
import inputbox
import socket
import pickle

# Connexion au serveur
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server = 'localhost'
port = 44444
addr = (server, port)
s.connect(addr)
data = s.recv(2048)
mainClock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption('game base')
screen = pygame.display.set_mode((950, 536))

font = pygame.font.SysFont(None, 20)
arriereplan = pygame.image.load('assets/image/menu/menu.PNG')
black = (0, 0, 0)

data = dict()

# Fonction qui simplifie la creation de text


def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, 1, color)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)


# Demande pseudo (Premier ecran du menu)
def login_menu():
    data['name'] = inputbox.login()
    s.send(pickle.dumps(data))
    # envoie pseudo serveur
    playerInfo = pickle.loads(s.recv(2048))
    choixplayer(playerInfo)


def text_objects(text, color, size="small"):

    if size == "small":
        textSurface = font.render(text, True, color)
    if size == "medium":
        textSurface = font.render(text, True, color)
    if size == "large":
        textSurface = font.render(text, True, color)

    return textSurface, textSurface.get_rect()


def text_to_button(msg, color, buttonx, buttony, buttonwidth, buttonheight, size="small"):
    textSurf, textRect = text_objects(msg, color, size)
    textRect.center = ((buttonx+(buttonwidth/2)), buttony+(buttonheight/2))
    screen.blit(textSurf, textRect)

# Choix skin du Joueur (Deuxieme ecran du menu)


def choixplayer(playerInfo):
    click = False
    while True:

        screen.blit(arriereplan, (0, 0))

        draw_text('mainUser menu', font, (255, 255, 255), screen, 20, 20)

        mx, my = pygame.mouse.get_pos()

        player_1 = pygame.Rect(50, 100, 200, 50)

        player_2 = pygame.Rect(50, 200, 200, 50)

        if player_1.collidepoint(mx, my):
            player = pygame.image.load("assets/image/player/player1Front2.png")
            if click:
                mainUser_menu(playerInfo)
        if player_2.collidepoint(mx, my):
            player = pygame.image.load("assets/image/player/player2Front2.png")
            if click:
                mainUser_menu(playerInfo)

        pygame.draw.rect(screen, (255, 255, 255), player_1)
        pygame.draw.rect(screen, (255, 255, 255), player_2)

        text_to_button("Player 1", black, 50, 100, 200, 50)
        text_to_button("Player 2", black, 50, 200, 200, 50)

        click = False
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)

# Menu principale (Troisieme ecran du menu)


def mainUser_menu(playerInfo):
    click = False
    while True:

        screen.blit(arriereplan, (0, 0))
        draw_text('mainUser menu', font, (255, 255, 255), screen, 20, 20)

        player1 = pygame.image.load("assets/image/player/player1Front2.png")
        player2 = pygame.image.load("assets/image/player/player3Front1.png")

        screen.blit(player1, (500, 50))
        screen.blit(player2, (600, 50))

        mx, my = pygame.mouse.get_pos()

        button_1 = pygame.Rect(50, 100, 200, 50)
        button_2 = pygame.Rect(50, 200, 200, 50)

        button_3 = pygame.Rect(50, 300, 200, 50)

        if playerInfo[1] == 'admin':
            button_4 = pygame.Rect(50, 400, 200, 50)

        # Si l'utilisateur appuie sur un bouton
        if button_1.collidepoint((mx, my)):
            if click:
                pve()
        if button_2.collidepoint((mx, my)):
            if click:
                pvp()
        if button_3.collidepoint((mx, my)):
            if click:
                howtoplay()
        if playerInfo[1] == 'admin':  # Si l'utilisateur est un admin
            if button_4.collidepoint((mx, my)):
                if click:
                    admin()

        # Créer des carrés blanc / surface des boutons
        pygame.draw.rect(screen, (255, 255, 255), button_1)
        pygame.draw.rect(screen, (255, 255, 255), button_2)
        pygame.draw.rect(screen, (255, 255, 255), button_3)
        if playerInfo[1] == 'admin':  # Si l'utilisateur est un admin
            pygame.draw.rect(screen, (255, 255, 255), button_4)

        # Créer des zones de text / Texte des boutons
        text_to_button("PVE", black, 50, 100, 200, 50)
        text_to_button("PVP", black, 50, 200, 200, 50)
        text_to_button("How to play", black, 50, 300, 200, 50)
        if playerInfo[1] == 'admin':    # Si l'utilisateur est un admin
            text_to_button("Admin", black, 50, 400, 200, 50)

        click = False
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)


def mainAdmin_menu():
    click = False
    while True:

        screen.blit(arriereplan, (0, 0))
        draw_text('mainAdmin menu', font, (255, 255, 255), screen, 20, 20)

        player1 = pygame.image.load("assets/image/player/player1Front2.png")
        player2 = pygame.image.load("assets/image/player/player3Front2.png")

        screen.blit(player1, (500, 50))
        screen.blit(player2, (600, 50))

        mx, my = pygame.mouse.get_pos()

        button_1 = pygame.Rect(50, 100, 200, 50)
        button_2 = pygame.Rect(50, 200, 200, 50)
        button_3 = pygame.Rect(50, 300, 300, 50)
        if button_1.collidepoint((mx, my)):
            if click:
                pve()
        if button_2.collidepoint((mx, my)):
            if click:
                pvp()
        if button_3.collidepoint((mx, my)):
            if click:
                configuration()
        pygame.draw.rect(screen, (255, 0, 0), button_1)
        pygame.draw.rect(screen, (255, 0, 0), button_2)
        pygame.draw.rect(screen, (255, 0, 0), button_3)

        text_to_button("PVE", black, 50, 100, 200, 50)
        text_to_button("PVP", black, 50, 200, 200, 50)
        text_to_button("How to play", black, 50, 300, 200, 50)

        click = False
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)

# Lance le jeu en PVE


def pve():
    running = True
    while running:
        import pve


# Lance le jeu en PVP
def pvp():
    running = True
    while running:
        import pvp

# Regle du jeu
def howtoplay():
    running = True
    while running:
        screen.blit(arriereplan, (0, 0))

        draw_text('howtoplay', font, (255, 255, 255), screen, 20, 20)

        draw_text('Commande : W A S D clavier querty, barre d\'espace pour lancer les sorts.',
                  font, (255, 255, 255), screen, 20, 100)

        draw_text('Commande : Z Q S D clavier azerty, barre d\'espace pour lancer les sorts.',
                  font, (255, 255, 255), screen, 20, 200)

        draw_text('Tapez echap pour retourner au menu principal.',
                  font, (255, 255, 255), screen, 20, 500)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False

        pygame.display.update()
        mainClock.tick(60)

# Menu admin


def admin():
    # envoie au serveur une demande (Demande toutes les potions en BDD)
    s.send(pickle.dumps({'crud': 'all'})) 
    allPotions = pickle.loads(s.recv(2048))# Reçois les données
    running = True
    while running:
        screen.blit(arriereplan, (0, 0))
        draw_text('Modifications des potions', font,
                  (255, 255, 255), screen, 20, 50)
        move = 200
        # Pour chaque potions affiche un template
        for potion in allPotions:
            draw_text(f'Nom: {potion[1]}, effet: {potion[3]} [nom du fichier: {potion[2]}]',
                      font, (255, 255, 255), screen, 20, move)
            move += 50
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
        click = False
        pygame.display.update()
        mainClock.tick(60)


login_menu()
